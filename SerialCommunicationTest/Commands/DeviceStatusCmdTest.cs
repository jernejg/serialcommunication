﻿using System.Text;
using Ploeh.AutoFixture;
using SerialCommunication.Commands;
using Xunit;
using Xunit.Extensions;

namespace SerialCommunicationTest.Commands
{
    public class DeviceStatusCmdTest : Fixture
    {
        [Theory,    InlineData("RECORDERS_STATUS;0;0;0\n\r", 0,0,0),
                    InlineData("RECORDERS_STATUS;0;1;2\n\r", 0, 1, 2)]
        public void Success_Argument_Is_Parsed_Correctly(string data, int general,int wave, int transient)
        {
            var sut = this.Create<DeviceStatusCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(data));

            Assert.Equal((int)sut.SuccessResult.General, general);
            Assert.Equal((int)sut.SuccessResult.Waveform, wave);
            Assert.Equal((int)sut.SuccessResult.Transient, transient);
        }
    }
}