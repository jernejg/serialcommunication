﻿using System.Text;
using Ploeh.AutoFixture;
using SerialCommunication.Commands;
using Xunit;
using Xunit.Extensions;

namespace SerialCommunicationTest.Commands
{
    public class TellDeviceToSeekInAFile : Fixture
    {
        [Theory,    InlineData("OK\n\r", true),
                    InlineData("OK", false),
                    InlineData(".OK\n\r.", true),
                    InlineData("ERROR\n\r", true),
                    InlineData(".ERROR\n\r.", true),
                    InlineData("ERRROR\n\r", false),
                    InlineData("ERRROR\n", false)
]
        public void CommandIsFinishedTest(string data, bool shouldSucceed)
        {
            var sut = this.Create<SeekFileCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(data));

            Assert.Equal(sut.IsFinished, shouldSucceed);
        }
    }
}