﻿using System;
using System.Linq;
using System.Text;
using Ploeh.AutoFixture;
using SerialCommunication.Commands;
using Xunit;
using Xunit.Extensions;

namespace SerialCommunicationTest.Commands
{
    public class TellDeviceToReadABlockOfBytes : Fixture
    {
        [Theory, InlineData("ERROR\n\r", "ERROR")]
        public void Error_Is_Returned(string data, string returnArg)
        {
            var sut = this.Create<ReadBlockCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(data));

            Assert.Equal(sut.ErrorResult, returnArg);
        }

        [Theory, InlineData("OK;1;!1a2b\n\ra", 1, 6699),
                 InlineData("OK;5;!dccd\n\r12345", 5, 56525)]
        public void Header_Is_Correctly_Parsed(string inputData, int correctBytesPromised, int correctCrc)
        {
            var sut = this.Create<ReadBlockCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(inputData));

            Assert.Equal(sut.SuccessResult.InstrumentCRC, correctCrc);
            Assert.Equal(sut.SuccessResult.ActualData.Count(), correctBytesPromised);
        }

        [Theory, InlineData("OK;1;!1a2b\n\raa")]
        public void Instrument_Returnes_More_Bytes_Than_Promised(string inputData)
        {
            var sut = this.Create<ReadBlockCmd>();
            Assert.Throws<OverflowException>(() => sut.Proccess(Encoding.ASCII.GetBytes(inputData)));
        }
    }
}