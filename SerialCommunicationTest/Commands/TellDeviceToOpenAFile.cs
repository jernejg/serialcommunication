﻿using System.Text;
using Ploeh.AutoFixture;
using SerialCommunication.Commands;
using Xunit;
using Xunit.Extensions;

namespace SerialCommunicationTest.Commands
{
    public class TellDeviceToOpenAFile : Fixture
    {
        [Theory, InlineData("OK;1\n\r",true),
                 InlineData("OK;1",false),
                 InlineData("ERROR\n\r",true),
                 InlineData("ERRROR\n\r",false),
                 InlineData("ERRROR\n",false)]
        public void CommandIsFinishedTest(string data,bool shouldSucceed)
        {
            var sut = this.Create<OpenFileCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(data));

            Assert.Equal(sut.IsFinished,shouldSucceed);
        }
    }
}
