﻿using System.Text;
using Ploeh.AutoFixture;
using SerialCommunication.Commands;
using Xunit;
using Xunit.Extensions;

namespace SerialCommunicationTest.Commands
{
    public class TellDeviceToCloseAFile : Fixture
    {

        [Theory, InlineData("OK;1\n\r", true),
        InlineData("OK;1", false),
        InlineData("OK;1\n\r.", true),
        InlineData("ERROR\n\r", true),
        InlineData("ERRROR\n\r", false),
        InlineData("ERRROR\n", false)
        ]
        public void Command_Is_Finished(string data, bool shouldSucceed)
        {
            var sut = this.Create<CloseFileCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(data));

            Assert.Equal(sut.IsFinished, shouldSucceed);
        }

        [Theory, InlineData("OK;1\n\r", 1),
                 InlineData("OK;2\n\r", 2)
        ]
        public void Success_Argument_Is_Parsed_Correctly(string data, int returnArg)
        {
            var sut = this.Create<CloseFileCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(data));

            Assert.Equal(sut.SuccessResult, returnArg);
        }

        [Theory, InlineData("ERROR\n\r")]
        public void Error_Argument_Is_Parsed_Correctly(string data)
        {
            var sut = this.Create<CloseFileCmd>();
            sut.Proccess(Encoding.ASCII.GetBytes(data));

            Assert.Equal(sut.ErrorResult, "ERROR");
        }

    }
}