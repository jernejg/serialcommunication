﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SerialCommunication.Commands;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.CommandGroup
{
    public class Lambdas
    {
        private readonly IDevice _device;
        private readonly DeviceFileInfo _fileInfo;
        private readonly IDataProccessor _dataProccessor;

        public Lambdas(IDevice device, DeviceFileInfo fileInfo, IDataProccessor dataProccessor)
        {
            _device = device;
            _fileInfo = fileInfo;
            _dataProccessor = dataProccessor;
        }

        public void Start()
        {
            var openFileCmd = new OpenFileCmd("R0151GEN.REC", FileAccess.Read);

            var openFileTask = _device.AddToExecutionQueue(openFileCmd);

            long bytesTransfered = 0;
            const int blockSize = 32 * 1024;


            var a = openFileTask.ContinueWith(fileOpened);

//            openFileTask.ContinueWith(x =>
//            {
//                var readBlockTask = _device.AddToExecutionQueue(new ReadBlockCmd(x.Result.SuccessResult, blockSize));
//
//                readBlockTask.ContinueWith(y =>
//                {
//                    var bytesRead = y.Result.SuccessResult.ActualData.ToArray();
//                    _dataProccessor.Proccess(bytesRead);
//                    bytesTransfered += bytesRead.Count();
//
////                    if (bytesTransfered < _fileInfo.Length)
//                       
//                });
//            });
        }

        private void fileOpened(Task<OpenFileCmd> task)
        {
            throw new NotImplementedException();
        }

//        public static Task<TResult> Retry<TResult>(Func<Task<TResult>> taskProvider, int maxAttemps, Func<Exception, bool> shouldRetry = null)
//        {
//            if (shouldRetry == null)
//                shouldRetry = ex => true;
//            return taskProvider().ContinueWith(task => RetryContinuation(task, taskProvider, maxAttemps, shouldRetry));
//        }
    }

    public static class Ext
    {
        public static void ContinueWithAndRepeatWhile<TResult>(this Task<TResult> me, Func<Task<TResult>> taskProvider, Func<bool> repeat)
        {
            me.ContinueWith(x =>
            {
                if (repeat())
                {
                    ContinueWithAndRepeatWhile(taskProvider(),taskProvider, repeat);
                }
            });
        }

//        private static async Task<T> Retry<T>(Func<T> func, int retryCount)
//        {
//            try
//            {
//                var result = await Task.Run(func);
//                return result;
//            }
//            catch
//            {
//                if (retryCount == 0)
//                    throw;
//            }
//            return await Retry(func, --retryCount);
//        }
    }
}