namespace SerialCommunication.CommandGroup
{
    public class DeviceFileInfo
    {
        public long Length { get; private set; }
        public string Name { get; private set; }

        public DeviceFileInfo(long length, string name)
        {
            Length = length;
            Name = name;
        }
    }
}