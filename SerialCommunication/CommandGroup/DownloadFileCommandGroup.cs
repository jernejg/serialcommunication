﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SerialCommunication.Commands;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.CommandGroup
{
    public class DownloadFileCommandGroup
    {
        private readonly DeviceFileInfo _fileInfo;
        private readonly IDevice _device;
        private readonly IDataProccessor _dataProccessor;

        public DownloadFileCommandGroup(DeviceFileInfo fileInfo,
            IDevice device,
            IDataProccessor dataProccessor)
        {
            _fileInfo = fileInfo;
            _device = device;
            _dataProccessor = dataProccessor;
        }

        public async Task Start()
        {
            long bytesTransfered = 0;
            const int blockSize = 32 * 1024;

            var openFileCmd = new OpenFileCmd(_fileInfo.Name, FileAccess.Read);
            OpenFileCmd fileHandleTask = await _device.AddToExecutionQueue(openFileCmd);

            int fileHandle = fileHandleTask.SuccessResult;

            while (bytesTransfered < _fileInfo.Length)
            {
                var readBlockCmd = new ReadBlockCmd(fileHandle, blockSize);
                ReadBlockCmd readBlockTask = await _device.AddToExecutionQueue(readBlockCmd);
                byte[] bytesRead = readBlockTask.SuccessResult.ActualData.ToArray();

                _dataProccessor.Proccess(bytesRead);
                bytesTransfered += bytesRead.Count();
            }

            var closeFileCmd = new CloseFileCmd(fileHandle);

            await _device.AddToExecutionQueue(closeFileCmd);
        }

        public Task<long> Start_Nested_Lambdas()
        {
            long bytesTransfered = 0;
            const int blockSize = 32 * 1024;

            var tcs = new TaskCompletionSource<long>();

            var openFileCmd = new OpenFileCmd(_fileInfo.Name, FileAccess.Read);
            Task<OpenFileCmd> fileHandleTask = _device.AddToExecutionQueue(openFileCmd);

            fileHandleTask.ContinueWith(x =>
            {
                int fileHandle = fileHandleTask.Result.SuccessResult;
                var readBlockCmd = new ReadBlockCmd(fileHandle, blockSize);
                Task<ReadBlockCmd> readBlockTask = _device.AddToExecutionQueue(readBlockCmd);

                //We would need to put this one into the loop
                readBlockTask.ContinueWith(blocks =>
                {
                    byte[] bytesRead = blocks.Result.SuccessResult.ActualData.ToArray();

                    _dataProccessor.Proccess(bytesRead);
                    bytesTransfered += bytesRead.Count();

                    var closeFileCmd = new CloseFileCmd(fileHandle);
                    Task<CloseFileCmd> closeHandle = _device.AddToExecutionQueue(closeFileCmd);

                    closeHandle.ContinueWith(closedHandle =>
                    {
                        tcs.SetResult(bytesTransfered);
                    });

                });


            });

            return tcs.Task;
        }

        public void StartSplit()
        {
            long bytesTransfered = 0;
            const int blockSize = 32 * 1024;

            var tcs = new TaskCompletionSource<long>();

            var openFileCmd = new OpenFileCmd(_fileInfo.Name, FileAccess.Read);
            Task<OpenFileCmd> fileHandleTask = _device.AddToExecutionQueue(openFileCmd);

            fileHandleTask.ContinueWith(FilehandleReceived);
        }

        private void FilehandleReceived(Task<OpenFileCmd> fileHandleTask)
        {
            int fileHandle = fileHandleTask.Result.SuccessResult;
            var readBlockCmd = new ReadBlockCmd(fileHandle, blockSize);
            Task<ReadBlockCmd> readBlockTask = _device.AddToExecutionQueue(readBlockCmd);
        }


        public static Task<TReturn> Retry<TReturn>(
            Func<Task<TReturn>> func,
            Func<TReturn, bool> condition,
            TaskCompletionSource<TReturn> tsc = null)
        {
            if (tsc == null)
                tsc = new TaskCompletionSource<TReturn>();

            func().ContinueWith(x =>
            {
                if (!condition(x.Result))
                    Retry(func, condition, tsc);
            });

            return tsc.Task;
        }


    }
}