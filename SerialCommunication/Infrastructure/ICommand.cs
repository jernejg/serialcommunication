﻿using System;
using System.Collections.Generic;

namespace SerialCommunication.Infrastructure
{
    public interface ICommand
    {
        IEnumerable<byte> CommandInstruction { get; }
        void Proccess(IEnumerable<byte> data);
        bool IsFinished { get; }
    }
}