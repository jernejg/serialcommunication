﻿using System;

namespace SerialCommunication.Infrastructure
{
    public interface ICommunicationPort
    {
        void Open();
        void Close();
        bool IsOpen();
        void Write(byte[] buffer);
        event EventHandler<CommunicationPortEventArgs> DataReceived;
    }
}