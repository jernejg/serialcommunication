﻿using System.Collections.Generic;

namespace SerialCommunication.Infrastructure
{
    public interface IDataProccessor
    {
        void Proccess(IEnumerable<byte> bytes);
    }
}