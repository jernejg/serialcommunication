﻿using System.Collections.Generic;

namespace SerialCommunication.Infrastructure
{
    public class CommandBase<TSuccessResult, TErrorResult> : CommandBase
    {
        public TErrorResult ErrorResult { get; protected set; }
        public TSuccessResult SuccessResult { get; protected set; }
    }

    public abstract class CommandBase
    {
        protected readonly List<byte> Buffer;
        public bool IsFinished { get; protected set; }

        protected CommandBase()
        {
            Buffer = new List<byte>();
        }
    }
}