﻿using System.Threading.Tasks;
using SerialCommunication.Device;

namespace SerialCommunication.Infrastructure
{
    public interface IDevice
    {
                Task<TReturn> AddToExecutionQueue<TReturn>(TReturn command) where TReturn :  class, ICommand;
//        void AddToExecutionQueue(CommandTaskCompletion command);
    }
}