﻿using System;

namespace SerialCommunication.Infrastructure
{
    public class CommunicationPortEventArgs : EventArgs
    {
        public byte[] Data { get; private set; }

        public CommunicationPortEventArgs(byte[] data)
        {
            Data = data;
        }
    }
}