﻿using System;
using System.IO.Ports;
using System.Text;
using SerialCommunication.Infrastructure;

namespace SerialCommunication
{
    public class ComPort : ICommunicationPort, IDisposable
    {
        public event EventHandler<CommunicationPortEventArgs> DataReceived;

        private readonly SerialPort _serialPort;

        public ComPort(string portName)
        {
            _serialPort = new SerialPort
            {
                ReadTimeout = SerialPort.InfiniteTimeout,
                ReadBufferSize = 8192,
                WriteBufferSize = 2048,
                PortName = portName,
                BaudRate = 115200,
                DtrEnable = true
            };

            _serialPort.DataReceived += _serialPort_DataReceived;
        }

        protected virtual void OnDataReceived(byte[] buffer)
        {
            var handler = DataReceived;

            if (handler != null)
                handler(this, new CommunicationPortEventArgs(buffer));
        }

        void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var len = _serialPort.BytesToRead;

            if (len == 0)
                return;

            var buffer = new byte[len];
            _serialPort.Read(buffer, 0, len);

            OnDataReceived(buffer);
        }

        public void Open()
        {
            _serialPort.Open();
        }

        public void Close()
        {
            _serialPort.Close();
        }

        public bool IsOpen()
        {
            return _serialPort.IsOpen;
        }

        public void Write(byte[] buffer)
        {
            _serialPort.WriteLine(Encoding.ASCII.GetString(buffer));
        }

        public void Dispose()
        {
            if (_serialPort.IsOpen)
                _serialPort.Close();

            _serialPort.DataReceived -= _serialPort_DataReceived;
        }
    }
}