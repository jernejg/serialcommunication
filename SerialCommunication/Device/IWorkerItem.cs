﻿using System;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Device
{
    public interface IWorkerItem
    {
        void SetResult();
        void SetException(Exception e);
        ICommand Command { get;}
    }
}