﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Device
{
    public class Device : IDevice, IDisposable
    {
        private readonly ICommunicationPort _communicationPort;
        private readonly ConcurrentQueue<byte[]> _packetsQ;
        private readonly Queue<IWorkerItem> _commandQueue;
        private IWorkerItem _executingCommand;

        private readonly Timer _proccessCommandsTimer;
        private DateTime _lastPacketReceivedTime;

        public Device(ICommunicationPort communicationPort)
        {
            _proccessCommandsTimer = new Timer(20);
            _communicationPort = communicationPort;
            _communicationPort.DataReceived += _communicationPort_DataReceived;
            _packetsQ = new ConcurrentQueue<byte[]>();
            _commandQueue = new Queue<IWorkerItem>();
            _proccessCommandsTimer.Elapsed += ProccessTrigger;
        }



        void _communicationPort_DataReceived(object sender, CommunicationPortEventArgs e)
        {
            _packetsQ.Enqueue(e.Data);
            _lastPacketReceivedTime = DateTime.Now;
        }

        public Task<TReturn> AddToExecutionQueue<TReturn>(TReturn command) where TReturn : class, ICommand
        {
            var commandWorkItem = CommandTaskCompletion<TReturn>.Create(command);

            _commandQueue.Enqueue(commandWorkItem);

            if (_executingCommand == null && !_proccessCommandsTimer.Enabled)
                _proccessCommandsTimer.Start();

            return commandWorkItem.TaskCompletionSource.Task;
        }


        void ProccessTrigger(object sender, ElapsedEventArgs e)
        {
            Exception exception;
            _proccessCommandsTimer.Enabled = false;

            if (_executingCommand == null)
            {
                if (_commandQueue.Count > 0)
                {
                    _executingCommand = _commandQueue.Dequeue();
                    ClearBuffer();
                    _communicationPort.Write(_executingCommand.Command.CommandInstruction.ToArray());
                    _lastPacketReceivedTime = DateTime.Now;
                }
            }
            else if (ExecutingCommandIsFinished(out exception))
            {
                if (exception == null)
                    _executingCommand.SetResult();
                else
                    _executingCommand.SetException(exception);

                _executingCommand = null;
            }
            else if (ExecutiongCommandIsTimeout())
            {
                _executingCommand.SetException(new TimeoutException());
                _executingCommand = null;
            }

            _proccessCommandsTimer.Enabled = _executingCommand != null || _commandQueue.Count > 0;
        }



        private bool ExecutingCommandIsFinished(out Exception exception)
        {
            byte[] data;
            exception = null;

            while (_packetsQ.TryDequeue(out data))
            {
                try
                {
                    _executingCommand.Command.Proccess(data);
                }
                catch (Exception e)
                {
                    exception = e;
                }

                if (_executingCommand.Command.IsFinished || exception != null)
                    return true;
            }

            return false;
        }

        private bool ExecutiongCommandIsTimeout()
        {
            var secondsSinceLastPacket = (DateTime.Now - _lastPacketReceivedTime).Seconds;

            return secondsSinceLastPacket > 10;
        }

        private void ClearBuffer()
        {
            while (!_packetsQ.IsEmpty)
            {
                byte[] data;
                _packetsQ.TryDequeue(out data);
            }
        }


        public void Dispose()
        {
            _communicationPort.DataReceived -= _communicationPort_DataReceived;
            _proccessCommandsTimer.Dispose();
        }

    }
}

//In .NET 4.0 there is a new namespace on the block, and it is called System.Collections.Concurrent. 
//In this namespace you will find a pretty decent number of goodies that will help you to more easily 
//write application which can leverage multiple threads without having to resort to manual locking.


//TaskCompletionSource is used to create Task objects that don't execute code.
//
//They're used quite a bit by Microsoft's new async APIs - any time there's I/O-based asynchronous operations (or other non-CPU-based asynchronous operations, like a timeout). Also, any async Task method you write will use TCS to complete its returned Task.


