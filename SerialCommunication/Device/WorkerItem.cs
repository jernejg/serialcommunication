﻿using System;
using System.Threading.Tasks;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Device
{
    public class CommandTaskCompletion<T> : IWorkerItem where T : class, ICommand
    {
        public ICommand Command { get; private set; }
        public TaskCompletionSource<T> TaskCompletionSource { get; private set; }

        private CommandTaskCompletion(ICommand command, TaskCompletionSource<T> taskCompletionSource)
        {
            Command = command;
            TaskCompletionSource = taskCompletionSource;
        }

        public static CommandTaskCompletion<T> Create(T command)
        {
            var cmdInfo = new CommandTaskCompletion<T>(command, new TaskCompletionSource<T>());
            return cmdInfo;
        }

        public void SetResult()
        {
            TaskCompletionSource.SetResult((T)Command);
        }

        public void SetException(Exception e)
        {
            TaskCompletionSource.SetException(e);
        }

    }
}