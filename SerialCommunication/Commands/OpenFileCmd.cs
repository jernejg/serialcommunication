﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Commands
{
    public class OpenFileCmd : CommandBase<int, string>, ICommand
    {
        public FileAccess FileAccess { get; private set; }
        public string FileName { get; private set; }

        private const string SuccessRegex = "(?<=OK;)(\\d)(?=\\n\\r)";
        private const string ErrorRegex = "ERROR\\n\\r";

        public IEnumerable<byte> CommandInstruction
        {
            get
            {
                var cmd = string.Format("~SD;F_OPEN;{0};{1}\n\r", "R", FileName);
                return Encoding.ASCII.GetBytes(cmd);
            }
        }

        public OpenFileCmd(string fileName, FileAccess fileAccess)
        {
            FileName = fileName;
            FileAccess = fileAccess;
        }

        public void Proccess(IEnumerable<byte> data)
        {
            Buffer.AddRange(data);

            var finishedWithSuccessMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), SuccessRegex);
            var finishedWithErrorMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), ErrorRegex);

            if (finishedWithSuccessMatch.Success || finishedWithErrorMatch.Success)
                IsFinished = true;

            if (finishedWithSuccessMatch.Success)
                SuccessResult = int.Parse(finishedWithSuccessMatch.ToString());
            else if (finishedWithErrorMatch.Success)
                ErrorResult = "Error";
        }


    }
}