﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using SerialCommunication.Commands.Arguments;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Commands
{
    public class DeviceStatusCmd : CommandBase<StatusSuccessArgs, string>, ICommand
    {
        private const string SuccessRegex = "RECORDERS_STATUS;\\d;\\d;\\d\\n\\r";

        public IEnumerable<byte> CommandInstruction 
        {
            get
            {
                const string instr = "~RECORDERS_STATUS\n\r";
                return Encoding.ASCII.GetBytes(instr);
            }
        }

        public void Proccess(IEnumerable<byte> data)
        {
            Buffer.AddRange(data);

            var finishedWithSuccessMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), SuccessRegex);

            if (finishedWithSuccessMatch.Success)
            {
                IsFinished = true;

                var gen = (RecorderStatusEnum)int.Parse(Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()),"(?<=RECORDERS_STATUS;)(\\d)(?=;\\d;\\d\\n\\r)").ToString());
                var wav = (RecorderStatusEnum)int.Parse(Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()),"(?<=RECORDERS_STATUS;\\d;)(\\d)(?=;\\d\\n\\r)").ToString());
                var tra = (RecorderStatusEnum)int.Parse(Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()),"(?<=RECORDERS_STATUS;\\d;\\d;)(\\d)(?=\\n\\r)").ToString());

               SuccessResult = new StatusSuccessArgs(gen,wav,tra);
            }

        }
    }

    public enum RecorderStatusEnum
    {
        STOPED = 0,
        WAITING_FOR_TRIGGER = 1,
        RECORDING = 2,
        SAVING = 3
    }
}