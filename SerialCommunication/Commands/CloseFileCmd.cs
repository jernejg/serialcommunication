﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Commands
{
    public class CloseFileCmd : CommandBase<int, string>, ICommand
    {
        private readonly int _fileHandle;

        private const string SuccessRegex = "(?<=OK;)(\\d)(?=\\n\\r)";
        private const string ErrorRegex = "ERROR\\n\\r";

        public CloseFileCmd(int fileHandle)
        {
            _fileHandle = fileHandle;
        }

        public IEnumerable<byte> CommandInstruction
        {
            get
            {
                var instr = string.Format("~SD;F_CLOSE;{0}\n\r", _fileHandle);
                return Encoding.ASCII.GetBytes(instr);
            }
        }
        public void Proccess(IEnumerable<byte> data)
        {
            Buffer.AddRange(data);

            var finishedWithSuccessMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), SuccessRegex);
            var finishedWithErrorMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), ErrorRegex);

            if (finishedWithSuccessMatch.Success || finishedWithErrorMatch.Success)
                IsFinished = true;

            if (finishedWithSuccessMatch.Success)
                SuccessResult = int.Parse(finishedWithSuccessMatch.ToString());
            else if (finishedWithErrorMatch.Success)
                ErrorResult = "Error";
        }
    }
}