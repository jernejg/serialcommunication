﻿namespace SerialCommunication.Commands.Arguments
{
    public class StatusSuccessArgs
    {
        public RecorderStatusEnum General { get; private set; }
        public RecorderStatusEnum Waveform { get; private set; }
        public RecorderStatusEnum Transient { get; private set; }

        public StatusSuccessArgs(RecorderStatusEnum general, RecorderStatusEnum waveform, RecorderStatusEnum transient)
        {
            General = general;
            Waveform = waveform;
            Transient = transient;
        }
    }
}