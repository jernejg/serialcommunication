﻿using System.Collections.Generic;

namespace SerialCommunication.Commands.Arguments
{
    public class ReadFileSuccessArgs
    {
        public int InstrumentCRC { get; private set; }

        public IEnumerable<byte> ActualData { get; private set; }

        public ReadFileSuccessArgs(IEnumerable<byte> actualData, int instrumentCRC)
        {
            ActualData = actualData;
            InstrumentCRC = instrumentCRC;
        }

    
    }
}