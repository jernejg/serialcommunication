﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using SerialCommunication.Commands.Arguments;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Commands
{
    public class ReadBlockCmd : CommandBase<ReadFileSuccessArgs, string>, ICommand
    {
        private readonly int _fileHandle;

        private readonly int _bytesRequested;
        private int _bytesPromised;
        private int _crc;

        private int ActualBytesReceived 
        {
            get
            {
                return Buffer.Count;
            }
        }

        private bool _headerReceived;


        private const string HeaderRegex = "OK;\\d+;![a-fA-F0-9]{4}\\n\\r";
        private const string ErrorRegex = "ERROR\\n\\r";

        public ReadBlockCmd(int fileHandle,  int bytesRequested)
        {
            _fileHandle = fileHandle;
            _bytesRequested = bytesRequested;
        }

        public IEnumerable<byte> CommandInstruction
        {
            get
            {
                var instr = string.Format("~SD;F_READ;{0};{1}\n\r", _fileHandle, _bytesRequested);
                return Encoding.ASCII.GetBytes(instr);
            }
        }


        public void Proccess(IEnumerable<byte> data)
        {
            if (IsFinished)
                return;

            if (!_headerReceived)
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    Buffer.Add(data.ElementAt(i));

                    var headerMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), HeaderRegex);
                    var errorMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), ErrorRegex);

                    if (headerMatch.Success)
                    {
                        _headerReceived = true;
                        _bytesPromised = int.Parse(Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()),"(?<=OK;)(\\d+)(?=;![a-fA-F0-9]{4}\\n\\r)").ToString());
                        _crc = Convert.ToInt32(Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), "(?<=OK;\\d+;!)([a-fA-F0-9]{4})(?=\\n\\r)").ToString(), 16);
                        Buffer.Clear();
                        Proccess(new ArraySegment<byte>(data.ToArray(),i+1,data.Count()-i-1));
                        break;
                    }

                    if (errorMatch.Success)
                    {
                        IsFinished = true;
                        ErrorResult = "ERROR";
                    }
                }
            }
            else
            {
                Buffer.AddRange(data);

                if (ActualBytesReceived ==_bytesPromised)
                {
                    IsFinished = true;
                    SuccessResult = new ReadFileSuccessArgs(Buffer,_crc);
                }
                else if (ActualBytesReceived > _bytesPromised)
                    throw new OverflowException("Device returned more bytes than promised!");
            }
        }
    }
}