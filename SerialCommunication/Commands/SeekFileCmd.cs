﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using SerialCommunication.Infrastructure;

namespace SerialCommunication.Commands
{
    public class SeekFileCmd : CommandBase<string, string>, ICommand
    {
        private readonly uint _position;
        private readonly int _fileHandle;

        private const string SuccessRegex = "OK\\n\\r";
        private const string ErrorRegex = "ERROR\\n\\r";

        public SeekFileCmd(uint position, int fileHandle)
        {
            _position = position;
            _fileHandle = fileHandle;
        }

        public IEnumerable<byte> CommandInstruction
        {
            get
            {
                var instr = string.Format("~SD;F_SEEK;{0};{1}\n\r", _fileHandle, _position);
                return Encoding.ASCII.GetBytes(instr);
            }
        }

        public void Proccess(IEnumerable<byte> data)
        {
            Buffer.AddRange(data);

            var finishedWithSuccessMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), SuccessRegex);
            var finishedWithErrorMatch = Regex.Match(Encoding.ASCII.GetString(Buffer.ToArray()), ErrorRegex);

            if (finishedWithSuccessMatch.Success || finishedWithErrorMatch.Success)
                IsFinished = true;

            if (finishedWithSuccessMatch.Success)
                SuccessResult = finishedWithSuccessMatch.ToString();
            else if (finishedWithErrorMatch.Success)
                ErrorResult = "Error";
        }
    }
}