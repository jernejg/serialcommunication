using System.Collections.Generic;
using System.Diagnostics;
using SerialCommunication.Infrastructure;

namespace GUI
{
    internal class Proccessor : IDataProccessor
    {
        private int block = 0;
        public void Proccess(IEnumerable<byte> bytes)
        {
            Debug.WriteLine(block++);
        }
    }
}