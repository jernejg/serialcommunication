﻿using System;
using System.Windows.Forms;
using SerialCommunication;
using SerialCommunication.CommandGroup;
using SerialCommunication.Device;

namespace GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var communicationPort = new ComPort("COM1");
            communicationPort.Open();

            var device = new Device(communicationPort);


            var deviceFileInfo = new DeviceFileInfo(948224, "R0003GEN.REC");
            var dataProccessor = new Proccessor();

            var downloadFileCommandGroup = new DownloadFileCommandGroup(
                deviceFileInfo,
                device,
                dataProccessor
                );

            await downloadFileCommandGroup.Start();
        }
    }
}
